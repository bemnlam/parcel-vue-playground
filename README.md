# parcel-vue-playground

A study on how to create a simple Vue app using Parcel.

## Note on Parcel
When you don't have parcel and init a npm project:
```
yarn global add parcel-bundler
yarn init -y
```

## Project structure

```
❯ tree -L 2
.
├── README.md
└── src
    ├── App.vue
    ├── Other.vue
    ├── dist (files generated)
    ├── index.html
    ├── index.js
    ├── node_modules
    ├── package.json
    └── yarn.lock
```

`index.js` is the entrypoint of the app, which loads the `App.vue`. `Other.vue` is the child component loaded by `App.vue`.